# askomics-spreadsheet2RDF

Simple script for converting spreadsheets (CSV or TSV) into **typed** RDF.
We assume that the speadsheet's headers are askomics-compliant.

## Usage 

Usage: `spreadsheet2rdf.py <tabulatedFile>` (**NB:** make sure to adjust the value of the default prefix)

## Todo

- [x] handle CURIEs
- [x] generate RDFS/OWL schema in addition to the dataset
- [ ] generate a VoID description of the dataset (or call a VoID generator?)
