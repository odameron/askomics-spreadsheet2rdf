#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Quick and dirty converter of CSV/TSV files into RDF.

Usage: spreadsheet2rdf.py <file.tsv>
Generates:
- file.ttl
- file-schema.owl

dc:creator Olivier Dameron
dc:creator https://orcid.org/0000-0001-8959-7189
"""

import csv
import sys

def isfloat(value):
	try:
		float(value)
		return True
	except ValueError:
		return False

def generateURI(entityIdent):
	colonPos = entityIdent.find(':')
	if entityIdent.startswith("http"):
		entityURI = "<" + entityIdent + ">"
	elif colonPos != -1:
		(entityPrefix,entityLocalIdent) = entityIdent.split(':', maxsplit=1)
		if entityPrefix not in knownPrefixes.keys():
			if entityPrefix not in unknownPrefixes:
				unknownPrefixes.append(entityPrefix)
		entityURI = entityIdent
	else:
		entityURI = ":" + entityIdent
	return entityURI

#print "Number of arguments: " + str(len(sys.argv))
if len(sys.argv) != 2:
	print("Usage {} <tabulated file>".format(sys.argv[0]))
	sys.exit()


knownPrefixes = {}
knownPrefixes['rdf'] = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'
knownPrefixes['rdfs'] = 'http://www.w3.org/2000/01/rdf-schema#'
knownPrefixes['owl'] = 'http://www.w3.org/2002/07/owl#'
knownPrefixes['xml'] = 'http://www.w3.org/XML/1998/namespace'
knownPrefixes['xsd'] = 'http://www.w3.org/2001/XMLSchema#'
knownPrefixes['skos'] = 'http://www.w3.org/2004/02/skos/core#'
knownPrefixes['default'] = 'http://askomics.org/data/'

unknownPrefixes = []

prefixesTTL = ""
for (ns, uri) in knownPrefixes.items():
	#prefixesTTL += "@prefix {}: <{}> .\n".format("" if ns == "default" else ns, uri)
	if ns == "default":
		continue
	prefixesTTL += "@prefix {}: <{}> .\n".format(ns, uri)
prefixesTTL += "\n"
prefixesTTL += "@prefix : <{}> .\n".format(knownPrefixes["default"])
prefixesTTL += "\n\n"
print(prefixesTTL)


csvFileName = sys.argv[1]
fileExtension = csvFileName[-3:]
if fileExtension == "csv":
	delimiter = ","
elif fileExtension == "tsv":
	delimiter = "\t"
else:
	print("Usage {} <tabulated file>".format(sys.argv[0]))
	sys.exit()

ttlFileName = csvFileName[:-3] + "ttl"
ontologyFileName = csvFileName[:-4] + "-schema.owl"
#print("TSV file is: {}".format(tsvFileName))
#print("TTL file is: {}".format(ttlFileName))

with open(csvFileName) as csvFile:
	with open(ttlFileName, 'w') as rdfFile, open(ontologyFileName, 'w') as ontologyFile:
		rdfFile.write(prefixesTTL)
		ontologyFile.write(prefixesTTL)
		ontologyFile.write("<" + knownPrefixes['default'] + csvFileName + "> rdf:type owl:Ontology .\n")
		ontologyFile.write("\n")
		ontologyFile.write("\n")
		datareader = csv.reader(csvFile, delimiter=delimiter)
		headers = next(datareader)

		print("!!!" + str(headers) + "!!!")
		nbCol = len(headers)
		print("!!! nb headers: " + str(nbCol))
		refEntity = generateURI(headers[0])
		knownClasses = [refEntity]
		print("!!! class: " + refEntity)
		ontologyFile.write(refEntity + " rdf:type owl:Class .\n")
		ontologyFile.write(refEntity + " rdfs:label \"" + headers[0] + "\" .\n")
		for numCol in range(1, nbCol):
			ontologyFile.write("\n")
			sepIndex = headers[numCol].find("@")
			if sepIndex == -1:
				relationName = generateURI(headers[numCol])
				ontologyFile.write(relationName + " rdf:type owl:DatatypeProperty .\n")
				ontologyFile.write(relationName + " rdfs:domain " + refEntity + " .\n")
				ontologyFile.write(relationName + " rdfs:label \"" + headers[numCol] + "\" .\n")
			else:
				relationName = generateURI(headers[numCol][:sepIndex])
				objectClass = generateURI(headers[numCol][sepIndex+1:])
				if objectClass not in knownClasses:
					ontologyFile.write(objectClass + " rdf:type owl:Class .\n")
					ontologyFile.write(objectClass + " rdfs:label \"" + headers[numCol][sepIndex+1:] + "\" .\n")
					ontologyFile.write("\n")
				ontologyFile.write(relationName + " rdf:type owl:ObjectProperty .\n")
				ontologyFile.write(relationName + " rdfs:domain " + refEntity + " .\n")
				ontologyFile.write(relationName + " rdfs:range " + objectClass + " .\n")
				ontologyFile.write(relationName + " rdfs:label \"" + headers[numCol][:sepIndex] + "\" .\n")
		
		numRow = 0
		for row in datareader:
			numRow += 1
			# handles blank lines gracefully
			if row == []:
				continue
			nbCol = len(row)
			refEntity = generateURI(row[0])

			rdfFile.write("{} rdf:type {} .\n".format(refEntity, generateURI(headers[0])))
			for numCol in range(1, nbCol):
				#print("Column {}:\t{}".format(numCol,row[numCol]))
				sepIndex = headers[numCol].find("@")
				if sepIndex == -1:
					relationName = generateURI(headers[numCol])
					if isfloat(row[numCol]):
						rdfFile.write("{} {} \"{}\"^^xsd:float .\n".format(refEntity, relationName, row[numCol]))
						if numRow == 1:
							ontologyFile.write("\n")
							ontologyFile.write(relationName + " rdfs:range xsd:float .\n")
					else:
						rdfFile.write("{} {} \"{}\"^^xsd:string .\n".format(refEntity, relationName, row[numCol]))
						if numRow == 1:
							ontologyFile.write("\n")
							ontologyFile.write(relationName + " rdfs:range xsd:string .\n")
				else:
					relationName = generateURI(headers[numCol][:sepIndex])
					objectClass = generateURI(headers[numCol][sepIndex+1:])
					rdfFile.write("{} {} {} .\n".format(refEntity, relationName, generateURI(row[numCol])))
					rdfFile.write("{} rdf:type {} .\n".format(generateURI(row[numCol]), objectClass))
			rdfFile.write("\n")
			#break
		if len(unknownPrefixes) > 0:
			print("\nWARNING: the following prefixes were observed in the data from {} but are unknown. Random values were generated for them at the end of the RDF file. Fix the data or consider declaring the prefixes in the {} script".format(sys.argv[1], sys.argv[0]))
			rdfFile.write("\n")
			rdfFile.write("# UNKNOWN PREFIXES found in the spreadsheet {}\n".format(sys.argv[1]))
			for prefix in unknownPrefixes:
				print("UNKNOWN PREFIX: " + prefix)
				rdfFile.write("@prefix {}: <{}{}/> .\n".format(prefix, knownPrefixes["default"], prefix))
